//g++ -c draw_scene.cpp -I/usr/include/vtk-5.8
//g++ *.o -lvtkCommon -lvtkHybrid -lvtkGenericFiltering -lvtkVolumeRendering -lvtksys -lvtkFiltering -lvtkRendering -lvtkIO -lvtkGraphics -lvtkexoIIc  -lvtkImaging -lvtkftgl  -lvtkDICOMParser  -o prog
#include "vtkPolyDataReader.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"
#include "vtkPointData.h"
#include "vtkFloatArray.h"
#include "vtkLongArray.h"
#include "vtkIntArray.h"
#include "vtkPolyData.h"
#include "vtkPolyDataWriter.h"
#include "vtkSmartPointer.h"

#include "MicrotubulePool.hpp"
#include "ElementPool.hpp"
#include "Microtubule.hpp"
#include "Element.hpp"
#include "drawScene.hpp"
#include "Contour.hpp"
#include <sstream>

using namespace std;

#define verbose 0
#define deb(x) if (verbose==1){cout << x << endl;}

void drawScene::pool2vtk(string s_filename, ElementPool* ep, MicrotubulePool* mp, int detail, int temps)
{
	//vtkSmartPointer<vtkPolyDataReader> contour = vtkSmartPointer<vtkPolyDataReader>::New();
	//contour->SetFileName("contour.vtk");
	//contour->Update();
	//
	//vtkSmartPointer<vtkTransform> translation =	vtkSmartPointer<vtkTransform>::New();
	//translation->Translate(250.0, 250.0, 250.0);
	//translation->Scale(500.0, 500.0, 500.0);
	//
	//
	//vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
	//transformFilter->SetInput(contour->GetOutput());
	//transformFilter->SetTransform(translation);
	//transformFilter->Update();
	//
	//
	//
	// créer un flux de sortie
    ostringstream oss;
    // écrire un nombre dans le flux
    oss << temps;
    // récupérer une chaîne de caractères
    string result = oss.str();
    s_filename+=result+".vtk";
    ofstream myfile;
    char *filename = (char*)s_filename.c_str();
    myfile.open(filename);
    
    vtkPolyData *total = vtkPolyData::New();
    vtkPoints *points = vtkPoints::New();
    vtkFloatArray *directions = vtkFloatArray::New();
    directions->SetNumberOfComponents(3);
    vtkIntArray *identite = vtkIntArray::New();
    identite->SetName("identite");
    vtkIntArray *contact = vtkIntArray::New();
    contact->SetName("contact");
    //vtkLongArray *space = vtkLongArray::New();
    //space->SetName("space");
    //vtkLongArray *space2 = vtkLongArray::New();
    //space2->SetName("space2");
    vtkIntArray *type = vtkIntArray::New();
    type->SetName("type");

    vtkIntArray *cortical = vtkIntArray::New();
    cortical->SetName("cortical");
    
    vtkIntArray *id_vtk = vtkIntArray::New();
    id_vtk->SetName("id_vtk");
    
    vtkIntArray *to_cut = vtkIntArray::New();
    to_cut->SetName("to_cut");
    //
    vtkFloatArray *nearest_contour_d = vtkFloatArray::New();
    nearest_contour_d->SetName("nearest_contour_d");
    
    vtkFloatArray *nearest_contour_d2 = vtkFloatArray::New();
    nearest_contour_d2->SetName("nearest_contour_d2");
    //vtkLongArray *nearest_contour = vtkLongArray::New();
    //nearest_contour->SetName("nearest_contour");
    
    vtkFloatArray *nearest_contour_vtk = vtkFloatArray::New();
    nearest_contour_vtk->SetName("nearest_contour_vtk");
    
    int i=0;
    deb("drawScene::pool2vtk | ep->getListElements()");
    list<Element *> lm_provi= ep->getListElements();
    Element *el;
    for (list<Element *>::iterator it = lm_provi.begin();it != lm_provi.end();it++)
    {
        deb("drawScene::pool2vtk | (detail == 0)");
		if (detail == 0)
		{
            deb("drawScene::pool2vtk | el = *it;");
			el = *it;
            deb("drawScene::pool2vtk | el->isAlive()");
            if (el->isAlive()){
                //cout << *el << endl;
                deb("drawScene::pool2vtk | (detail == 0) | getAnchor()");
                Anchor* pos=el->getAnchor();
                float pos_[3] = {(float)*(pos->getX()), (float)*(pos->getY()), (float)*(pos->getZ())};
                vector<double> dir=el->getShape().getDirection();
                float dir_[3] = {(float)dir[0], (float)dir[1], (float)dir[2]};
                deb("drawScene::pool2vtk | (detail == 0) | getPositionContour(*it)");
//                 long c=ep->getPositionContour(*it);
//                 long c2=ep->getPositionMt(*it);
                //cout << *it<<"\t"<<pos_[0]<<"\t"<<pos_[1]<<"\t"<<pos_[2] <<"\t"<<spa[0]<<"\t"<<spa[1]<<"\t"<<spa[2]<<"\t"<< c <<endl;
                deb("drawScene::pool2vtk | (detail == 0) | InsertPoint");
                points->InsertPoint(i,pos_);
#if defined VTK_VERSION_7_1PLUS
		directions->InsertTypedTuple(i,dir_); //vtk 7.1 update             
#else
                directions->InsertTupleValue(i,dir_); //deprecated vtk 7.1
#endif
                deb("drawScene::pool2vtk | (detail == 0) | el->getStructureType()");
                //cout << (int)(el->getStructureType()) << endl;
                deb("drawScene::pool2vtk | (detail == 0) | el->getStructure()->getId())");
                identite->InsertValue(i,(int)(el->getStructureId()));
                deb("drawScene::pool2vtk | (detail == 0) | (int)(el->getPropriete('contact')");
                //space->InsertValue(i,c);
                //space2->InsertValue(i,c2);
                contact->InsertValue(i, (int)(el->getPropriete("contact")));
                type->InsertValue(i, (int)(el->getStructureType()));
                //nearest_contour->InsertValue(i, (int)(el->getPropriete("nearest_contour")));
                nearest_contour_d->InsertValue(i, (float)(el->getPropriete("nearest_contour_d")));
                nearest_contour_d2->InsertValue(i, (float)(el->getPropriete("D_pos_dir2lim_mbFn")));
                nearest_contour_vtk->InsertValue(i, (float)(el->getPropriete("nearest_contour_vtk")));
                to_cut->InsertValue(i, (el->getPropriete("to_cut")));
                id_vtk->InsertValue(i, (el->getId_vtk()));
                cortical->InsertValue(i, (el->getPropriete("cortical")));
                i++;
            }
            
		}
		else
		{
            deb("drawScene::pool2vtk | else");
			el = *it;
            if (el->isAlive()){
                Anchor* pos=el->getAnchor();
                float pos_[3] = {(float)*(pos->getX()), (float)*(pos->getY()), (float)*(pos->getZ())};
                //deb("toto 1c");
                vector<double> dir=el->getShape().getDirection();
                float dir_[3] = {(float)dir[0], (float)dir[1], (float)dir[2]};
                //cout << *it <<"\t"<<el->getStructure()<<"\t"<< pos[0] <<endl;
                deb("toto 1b");
//                 long c=ep->getPositionContour(*it);
//                 long c2=ep->getPositionMt(*it);
                //cout << *it<<"\t"<<pos_[0]<<"\t"<<pos_[1]<<"\t"<<pos_[2] <<"\t"<<spa[0]<<"\t"<<spa[1]<<"\t"<<spa[2]<<"\t"<< c <<endl;
                //points->InsertPoint(i,pos_);
                //directions->InsertTupleValue(i,dir_);
                //identite->InsertValue(i,(int)(el->getStructure()));
                if ( (int)(el->getStructureType()) == 1)
                {
                    points->InsertPoint(i,pos_);
#if defined VTK_VERSION_7_1PLUS
		    directions->InsertTypedTuple(i,dir_); //vtk 7.1 update             
#else
                    directions->InsertTupleValue(i,dir_); //deprecated vtk 7.1
#endif
                    identite->InsertValue(i,(int)(el->getStructureId()));
                    //space->InsertValue(i,c);
                    //space2->InsertValue(i,c2);
                    contact->InsertValue(i, (int)(el->getPropriete("contact")));
                    type->InsertValue(i, el->getStructureType());
                    
                    //nearest_contour->InsertValue(i, (int)(el->getPropriete("nearest_contour")));
                    nearest_contour_d->InsertValue(i, (float)(el->getPropriete("nearest_contour_d")));
                    nearest_contour_d2->InsertValue(i, (float)(el->getPropriete("D_pos_dir2lim_mbFn")));
                    nearest_contour_vtk->InsertValue(i, (float)(el->getPropriete("nearest_contour_vtk")));
                    to_cut->InsertValue(i, (el->getPropriete("to_cut")));
                    id_vtk->InsertValue(i, (el->getId_vtk()));
                    cortical->InsertValue(i, (el->getPropriete("cortical")));
                    i++;
                }
            }
		}

    }
    deb("toto 1c");
    //cout<<"draw2"<<endl;
    
    total->SetPoints(points);
    points->Delete();    
    total->GetPointData()->SetScalars(directions);
    directions->Delete();    
    total->GetPointData()->AddArray(identite);
    identite->Delete();        
    total->GetPointData()->AddArray(type);
    type->Delete();
    
    
    //total->GetPointData()->AddArray(space);
    //space->Delete();
    //total->GetPointData()->AddArray(space2);
    //space2->Delete();
    total->GetPointData()->AddArray(contact);
    contact->Delete();
    total->GetPointData()->AddArray(to_cut);
    to_cut->Delete();
    
    total->GetPointData()->AddArray(id_vtk);
    id_vtk->Delete();
    
    //total->GetPointData()->AddArray(nearest_contour);
    //nearest_contour->Delete();
    
    total->GetPointData()->AddArray(nearest_contour_d);
    nearest_contour_d->Delete();
    
    total->GetPointData()->AddArray(nearest_contour_vtk);
    nearest_contour_vtk->Delete();
    
    
    total->GetPointData()->AddArray(nearest_contour_d2);
    nearest_contour_d2->Delete();
    
    total->GetPointData()->AddArray(cortical);
    cortical->Delete();
    
    vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
//     writer->SetInput(total);
    writer->SetInputData(total);
    //cout<<"draw3"<<endl;
    deb("toto 2");
    writer->SetFileName(filename);
    writer->SetFileTypeToBinary();
    writer->Write();
    
    //writer = vtkSmartPointer<vtkPolyDataWriter>::New();
    //writer->SetInput(transformFilter->GetOutput());
    //cout<<"draw3"<<endl;
    //writer->SetFileName("contourAdapte.vtk");
    //writer->Write();
    
    myfile.close();
}
