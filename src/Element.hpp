//classe Element
//non si element.h deja importe ailleurs
#ifndef __ELEMENT_H_INCLUDED__
#define __ELEMENT_H_INCLUDED__
#include <iostream>
#include <vector>
#include <unordered_map>
#include "Structure.hpp"
#include "Shape.hpp"
#include "Anchor.hpp"
#include "Parametres.hpp"
#include <string>


//definition necessaire pour le pointeur
class ElementPool;
class Space;
class Element;
class Structure;
typedef int Id;

class Element
{
public:
    friend std::ostream& operator<<(std::ostream& os, Element p);
    Element();
    Element(Parametres *params, ElementPool* ep, Id id);

    ~Element();

    Shape getShape() const;
    void setShape(Shape shape);

    Anchor* getAnchor() ;
    void setAnchor(Anchor anchor);

    Id getId() const;
    void setId(Id id);
    
    Id getId_vtk() const;
    void setId_vtk(Id id);
    
    Id getAge() const;
    void setAge(Id age);
    
    
    
    void setPropriete(std::string p, double k);
    void setPropriete(std::string p, std::vector<double> k);
	double getPropriete(std::string p);
    std::vector<double> getPropriete_v(std::string p);
    void setDead();
    int isAlive();

    
    void setStructure(Structure* id);
    Structure* getStructure();
    int getStructureType() const;
    int getStructureId() const;
    int isMember() const;
    Element* getPrevious();
    void setPrevious(Element* previous);
    

/*
 * deux comportements de base :
 * - decay : ne disparait pas mais un parametre decroit (non utilise pour le moment)
 * - increase : accroit un parametre (non utilise pour le moment)
 */

    void decay();
    void increase();    
    

private:
    //on peut imaginer y ajouter des trucs genre "forme"    
    Shape m_shape;
    Anchor m_anchor;
    //l'id
    Id m_id;
    //l'id vtk
    Id m_id_vtk;
    //age
    int m_age;
    //present dans l'espace
    int m_alive;
    //le pool createur
    ElementPool *m_ep;
    Parametres *m_params;
    //la structure container
    Structure* m_structure;
    int m_structure_id;
    int m_structure_type;
    Element *m_previous;
    //un dictionnaire de caractéristiques
    std::unordered_map<std::string, double> m_proprietes;
    std::unordered_map<std::string, std::vector<double> > m_proprietes_v;
    
    
};
#endif

