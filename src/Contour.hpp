//classe Contour
//non si Contour.h deja importe ailleurs
#ifndef __CONTOUR_H_INCLUDED__
#define __CONTOUR_H_INCLUDED__
#include <iostream>
#include "vtkPolyDataReader.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"
#include "vtkPolyDataWriter.h"
#include <unordered_map>
#include <string>
#include "Element.hpp"
#include "Structure.hpp"
#include <deque>
#include <vector>

class ElementPool;
class Parametres;

class Contour:public Structure
{
	friend std::ostream& operator<<(std::ostream& os, Contour& m);
	public:
		Contour(Parametres *params, ElementPool *ep, Id r);
		void open();
		virtual std::ostream& Print( std::ostream &os) const;
		std::vector<double> getElementRandom();


		
		
	private:
		ElementPool *m_elementPool;
                Parametres *m_params;
		std::deque<Element *> m_body;
		
};
#endif

